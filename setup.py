import setuptools
import json
import os
import shutil
import torch
import onnxruntime
import onnx
import numpy as np
import tensorflow as tf
import torch.nn.functional as F
import tensorflow as tf
from onnx_tf.backend import prepare
import cdpam


def gen_onnx():
    loss_fn = cdpam.CDPAM()

    wav_ref = np.random.randn(1,20000)
    wav_out = np.random.randn(1,20000)


    audio1 = torch.from_numpy(wav_ref).float().to("cuda")
    audio1 =audio1.unsqueeze(1) 

    audio2 = torch.from_numpy(wav_out).float().to("cuda")
    audio2 =audio2.unsqueeze(1)

    _,a1,c1 = loss_fn.model.base_encoder.forward(audio1)
    _,a2,c1 = loss_fn.model.base_encoder.forward(audio2)

    a1 = F.normalize(a1, dim=1)
    a2 = F.normalize(a2, dim=1)

    torch.onnx.export(loss_fn.model.base_encoder,               
            audio1,                         
            "cdpam_tf/cdpam_encoder.onnx",   
            export_params=True,        
            opset_version=12,          
            do_constant_folding=True,  
            input_names = ['input'],   
            output_names = ["x","a","c"], 
            )
    torch.onnx.export(loss_fn.model.model_dist,               
            (a1,a2),                         
            "cdpam_tf/cdpam_dist.onnx",   
            export_params=True,        
            opset_version=12,          
            do_constant_folding=True,  
            input_names = ["a1","a2"],   
            output_names = ['dist'], 
            )

gen_onnx()

setuptools.setup(
    name="cdpam_tf",
    version="0.0.4",
    description="tf version of cdpam",
    packages=setuptools.find_packages(),
    package_data={'cdpam_tf': ['cdpam_tf/cdpam_dist.onnx','cdpam_tf/cdpam_encoder.onnx']},
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
