import numpy as np
import librosa
import tensorflow as tf
from onnx_tf.backend import prepare
import onnx
import inspect

def get_tf_cdpam(ref,out):
    dir_path = os.path.dirname(os.path.realpath(__file__)) 
    encoder = onnx.load(f'{dir_path}/cdpam_encoder.onnx')
    distance = onnx.load(f'{dir_path}/cdpam_dist.onnx')

    tf_encoder = prepare(encoder)
    tf_distance = prepare(distance)
    a1 = tf_encoder.run(ref).a
    a2 = tf_encoder.run(out).a
    a1 = tf.keras.utils.normalize(a1,axis=1)
    a2 = tf.keras.utils.normalize(a2,axis=1)
    return tf_distance.run((a1,a2))


if __name__=="__main__":
    wav_ref,sr= librosa.load('sample_audio/ref.wav')
    wav_out,sr= librosa.load('sample_audio/2.wav')

    wav_out *= 32768.0
    wav_ref *= 32768.0

    wav_ref = np.reshape(wav_ref,[1,-1])
    wav_out = np.reshape(wav_out,[1,-1])


    wav_ref_tf = np.expand_dims(wav_ref,axis=0)
    wav_out_tf = np.expand_dims(wav_out,axis=0)

    dist_tf = get_tf_cdpam(wav_ref_tf,wav_out_tf)
    print(dist_tf)
    pass

